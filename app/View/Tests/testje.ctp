<!-- begin Content -->
<section>
<div class="container">
	<div class="row">
		<div class="col-md-9 col-md-push-3 property-details big">
		   <?php foreach ($properties as $property) { ?>

				<div class="property-details-item">
					<figure class="item-thumbnail">
						<span class="overthumb"></span>
						<div class="icons"><a href="#"><i class="icon-plus"></i></a></div>
					</figure>

					<ul class="features">
						<li><span class="price">$ <?php echo $property['Property']['price']?></span></li>
						<li><i class="icon-battery"></i> <?php echo $property['Property']['bathrooms']?> Bathrooms</li>
						<li><i class="icon-keyboard"></i> <?php echo $property['Property']['bedrooms']?> Bedrooms</li>
						<li><a href="<?php echo Router::url(
						array('controller'=>'tests', 'action'=>'delete', $property['Property']['id']));
						?>"><button class="btn btn-details">Delete</button></a></li>
					</ul>

					<h5>The Beatriche Loft</h5>
					<p class="subtitle"><i class="icon-location"></i> Miami, Florida, USA</p>
					<p><?php echo $property['Property']['description']?></p>
				</div>
			<?php } ?>

			<div class="clearfix"></div>
		</div>
	</div>
</div>

</section>
<!-- end Content -->

</body>
</html>