<head>
	<!-- Define Charset -->
	<meta charset="utf-8"/>

	<!-- Page Title -->
	<title>Blog Single Post - Fullwidth</title>

	<!-- Responsive Metatag -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1" />

	<!-- <meta http-equiv="X-UA-Compatible" content="IE=9" />-->

	<!-- CSS -->

	<?php
	echo $this->Html->css(
	array('/frontcalipso/css/vendors/bootstrap.min.css',
	'/frontcalipso/css/vendors/fontello.css',
	'/frontcalipso/css/vendors/owl.carousel.css',
	'/frontcalipso/css/vendors/slider.css',
	'/frontcalipso/css/styles.css',
	'/frontcalipso/css/media-queries.css'
	)
	);
	?>


	<!-- Font icons -->
	<link rel="stylesheet" href="frontcalipso/css/vendors/fontello.css" >
	<!--[if IE 7]>
	<link rel="stylesheet" href="frontcalipso/css/fontello-ie7.css" ><![endif]-->

	<!-- Owl Carousel -->
	<link rel="stylesheet" href="frontcalipso/css/vendors/owl.carousel.css" >

	<link rel="stylesheet" href="frontcalipso/css/vendors/slider.css" >

	<!-- Custom CSS -->
	<link rel="stylesheet" href="frontcalipso/css/styles.css" />
	<!-- Custom Media-Queties -->
	<link rel="stylesheet" href="frontcalipso/css/media-queries.css" />

	<!-- HTML5 shim, for IE6-8 support of HTML5 elements -->
	<!--[if lt IE 9]>
		<script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
	<![endif]-->

	<!-- Media queries -->
	<!--[if lt IE 9]>
		<script src="http://css3-mediaqueries-js.googlecode.com/svn/trunk/css3-mediaqueries.js"></script>
	<![endif]-->
</head>
<body>
<!-- begin Header -->
<header>

<div class="header-top">
<div class="container">
	<div class="row">
	<div class="col-md-5 contact">
		<a href="#"><i class="icon-mail"></i>info@company.com</a>
		<a href="#"><i class="icon-phone"></i>+151 15154 4554</a>
	</div>
	<div class="col-md-7 right">
		<ul class="lang">
			<li><a href="#" class="spanish">Spanish</a></li>
			<li><a href="#" class="italian">Italian</a></li>
			<li><a href="#" class="english">English</a></li>
		</ul>
		<ul class="social">
			<li><a href="#" title="Vimeo"><i class="icon-vimeo"></i></a></li>
			<li><a href="#" title="Facebook"><i class="icon-facebook"></i></a></li>
			<li><a href="#" title="Pinterest"><i class="icon-pinterest"></i></a></li>
			<li><a href="#" title="Skype"><i class="icon-skype"></i></a></li>
			<li><a href="#" title="Google+"><i class="icon-gplus"></i></a></li>
			<li><a href="#" title="Linkedin"><i class="icon-linkedin"></i></a></li>
			<li><a href="#" title="Twitter"><i class="icon-twitter"></i></a></li>
		</ul>
		<form class="form-inline form-search" role="form">
			<div class="form-group">
				<label class="sr-only" for="search">Search</label>
				<i class="icon-search"></i>
				<input type="text" class="form-control" id="search" placeholder="Search here...">
			</div>
			<button type="submit" class="btn btn-default">Sign in</button>
		</form>
		<div class="options dropdown">
			<a data-toggle="dropdown" href="#" id="dLabel"><i class="icon-cog"></i></a>
			<ul class="dropdown-menu" role="menu" aria-labelledby="dLabel">
				<li><a href="#"><i class="icon-cog"></i> Settings</a></li>
				<li><a href="#"><i class="icon-user"></i> Edit Profile</a></li>
				<li><a href="#"><i class="icon-help"></i> Help</a></li>
				<li><a href="#"><i class="icon-reply"></i> Logout</a></li>
			</ul>
		</div>
	</div>
	</div>
</div>
</div>

<nav class="navbar" role="navigation">
<div class="container">
	<!-- Brand and toggle get grouped for better mobile display -->
	<div class="navbar-header">
		<button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
			<span class="sr-only">Toggle navigation</span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
			<span class="icon-bar"></span>
		</button>
		<a class="navbar-brand" href="#"><img src="img/logo.png" alt="//"></a>
	</div>

	<!-- Collect the nav links, forms, and other content for toggling -->
	<div class="collapse navbar-collapse">

		<ul class="nav navbar-nav">

			<li ><a href="index.html">Home</a></li>

			<li class="dropdown ">
				<a href="company.html" class="dropdown-toggle active" data-toggle="dropdown">Company<span class="caret"></span></a>
				<ul class="dropdown-menu">
					<li ><a href="company_left_sidebar.html">Company Left Sidebar</a></li>
					<li ><a href="company_right_sidebar.html">Company Right Sidebar</a></li>
				</ul>
			</li>

			<li class="dropdown ">
				<a href="rent.html" class="dropdown-toggle" data-toggle="dropdown">Rent<span class="caret"></span></a>
				<ul class="dropdown-menu">
					<li ><a href="rent_2_columns.html">Rent 2 Columns</a></li>
					<li ><a href="rent_left_sidebar.html">Rent Left Sidebar</a></li>
					<li ><a href="rent_right_sidebar.html">Rent Right Sidebar</a></li>
				</ul>
			</li>

			<li class="dropdown ">
				<a href="buy.html" class="dropdown-toggle" data-toggle="dropdown">Buy<span class="caret"></span></a>
				<ul class="dropdown-menu">
					<li ><a href="buy_left_sidebar.html">Buy Left Sidebar</a></li>
					<li ><a href="buy_right_sidebar.html">Buy Right Sidebar</a></li>
				</ul>
			</li>

			<li class="dropdown ">
				<a href="sell.html" class="dropdown-toggle" data-toggle="dropdown">Sell<span class="caret"></span></a>
				<ul class="dropdown-menu">
					<li ><a href="sell_left_sidebar.html">Sell Left Sidebar</a></li>
					<li ><a href="sell_right_sidebar.html">Sell Right Sidebar</a></li>
				</ul>
			</li>

			<li class="dropdown active">
				<a href="#" class="dropdown-toggle" data-toggle="dropdown">Pages <span class="caret"></span></a>
				<ul class="dropdown-menu">
					<li ><a href="property_single_left_sidebar.html">Property Single Left Sidebar</a></li>
					<li ><a href="property_single_right_sidebar.html">Property Single Right Sidebar</a></li>
					<li ><a href="invest_3_columns.html">Invest 3 columns</a></li>
					<li ><a href="invest_4_columns.html">Invest 4 columns</a></li>
					<li ><a href="blog_post_fullwidth.html">Blog Post Fullwidth</a></li>
					<li ><a href="blog_post_left_sidebar.html">Blog Post Left Sidebar</a></li>
					<li ><a href="blog_post_right_sidebar.html">Blog Post Right Sidebar</a></li>
					<li ><a href="pricing_tables.html">Pricing Tables</a></li>
					<li ><a href="progress_bar.html">Progress Bar</a></li>
					<li ><a href="error_404.html">Error 404</a></li>
				</ul>
			</li>

			<li class="dropdown">
				<a href="blog.html" class="dropdown-toggle" data-toggle="dropdown">News <span class="caret"></span></a>
				<ul class="dropdown-menu">
					<li ><a href="news_fullwidth.html">News Fullwidth</a></li>
					<li ><a href="news_left_sidebar.html">News Left Sidebar</a></li>
					<li ><a href="news_right_sidebar.html">News Right Sidebar</a></li>
				</ul>
			</li>

			<li class="dropdown ">
				<a href="contact.html" class="dropdown-toggle" data-toggle="dropdown">Contact<span class="caret"></span></a>
				<ul class="dropdown-menu">
					<li ><a href="contact_left_sidebar.html">Contact Left Sidebar</a></li>
					<li ><a href="contact_right_sidebar.html">Contact Right Sidebar</a></li>
				</ul>
			</li>

		</ul>
	</div><!--/.nav-collapse -->
</div>
</nav>
