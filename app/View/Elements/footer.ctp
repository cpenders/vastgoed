<!-- Footer -->
<footer>

<div class="footer-top">
<div class="container">

	<div class="row">
		<div class="col-sm-6 col-md-3">
			<h4>About Company</h4>
			<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque, provident quas accusamus deleniti velit magni necessitatibus quis eum dolorum error. Rem, maxime, et consequuntur sint provident unde similique. Eos, laudantium!</p>
			<address>
				<i class="icon-address"></i>Address 123, Country <br />
				<i class="icon-phone"></i>(555) 415 5846<br />
				<i class="icon-mail"></i><a href="#">email@company.com</a> <br />
			</address>
		</div>
		<div class="col-sm-6 col-md-3">
			<h4>Contact Information</h4>
			<address>
				<i class="icon-phone"></i>Phone: +555 5439543 <br />
				<i class="icon-phone"></i>Phone: +555 5439543 <br />
			</address>

			<address>
				<i class="icon-address"></i>Address 543 Street, Los Angeles <br />
				<i class="icon-mail"></i><a href="#">email@company.com</a> <br />
				<i class="icon-monitor"></i><a href="#">www.company.com</a> <br />
			</address>
		</div>
		<div class="clearfix visible-sm"></div>
		<div class="col-sm-6 col-md-3">
			<h4>Newsletter sing up</h4>
			<form class="form-suscribe" role="form">
				<div class="form-group">
					<label for="newsletter-name" class="sr-only">Enter your name</label>
					<input type="text" class="form-control" id="newsletter-name" placeholder="Enter your name">
				</div>
				<div class="form-group">
					<label for="newsletter-email" class="sr-only">Enter your e-mail</label>
					<input type="email" class="form-control" id="newsletter-email" placeholder="Enter your e-mail">
				</div>

				<button type="submit" class="btn btn-default"><i class="icon-forward"></i>Suscribe</button>
			</form>
		</div>
		<div class="col-sm-6 col-md-3">
			<h4>Come to visit us</h4>
			<div class="map" id="map"></div>
		</div>
	</div>
</div>
</div>


<div class="footer-bottom">
<div class="container">

	<div class="row">
		<div class="col-md-7 col-lg-8">
			<ul class="bottom-menu">
				<li ><a href="index.html">Home</a></li>
				<li ><a href="company.html">Company</a></li>
				<li ><a href="rent.html">Rent</a></li>
				<li ><a href="buy.html">Buy</a></li>
				<li ><a href="sell.html">Sell</a></li>
				<li ><a href="#">Pages</a></li>
				<li ><a href="blog.html">News</a></li>
				<li ><a href="contact.html">Contact</a></li>
			</ul>
		</div>
		<div class="col-md-5 col-lg-4 copy">
			<p>Copyright - <a href="#">Privacy Policy</a> - <strong>Company Name</strong> <a href="#" class="to-top"><i class="icon-up-circled"></i></a></p>
		</div>
	</div>

</div>
</div>

</footer>
<!-- end Footer -->


<!-- ******************************************************************** -->
<!-- ************************* Javascript Files ************************* -->
<!-- jQuery -->
<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>
<script>window.jQuery || document.write("<script src=\"frontcalipso/js/vendors/jquery-1.9.1.min.js\"")</script>

<!-- Respond.js media queries for IE8 -->
<script src="frontcalipso/js/vendors/respond.min.js"></script>

<!-- Bootstrap-->
<script src="frontcalipso/js/vendors/bootstrap.min.js" ></script>

<!-- Retina.js -->
<script src="frontcalipso/js/vendors/retina.js" ></script>

<!-- Placeholder.js -->
<!--[if lte IE 9]> <script src="frontcalipso/js/vendors/placeholder.js" ></script> <script>Placeholder.init();</script> <![endif]-->

<!-- Owl Carousel -->
<script src="frontcalipso/js/vendors/owl.carousel.min.js" ></script>

<!-- Gmaps -->
<script src="http://maps.google.com/maps/api/js?sensor=true" ></script>
<script src="frontcalipso/js/vendors/gmaps.js" ></script>

<!-- Range Slider -->
<script src="frontcalipso/js/vendors/bootstrap-slider.js" ></script>



<!-- Custom -->
<script src="frontcalipso/js/script.js"  ></script>

<!-- *********************** End Javascript Files *********************** -->
<!-- ******************************************************************** -->


