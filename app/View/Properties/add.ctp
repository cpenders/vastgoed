<div class="properties form">
<?php echo $this->Form->create('Property'); ?>
	<fieldset>
		<legend><?php echo __('Add Property'); ?></legend>
	<?php
		echo $this->Form->input('size');
		echo $this->Form->input('bedrooms');
		echo $this->Form->input('bathrooms');
		echo $this->Form->input('garages');
		echo $this->Form->input('price');
		echo $this->Form->input('yearbuilt');
		echo $this->Form->input('rent');
		echo $this->Form->input('sale');
		echo $this->Form->input('description');
	?>
	</fieldset>
<?php echo $this->Form->end(__('Submit')); ?>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>

		<li><?php echo $this->Html->link(__('List Properties'), array('action' => 'index')); ?></li>
	</ul>
</div>
