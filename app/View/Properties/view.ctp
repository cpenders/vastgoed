<div class="properties view">
<h2><?php echo __('Property'); ?></h2>
	<dl>
		<dt><?php echo __('Id'); ?></dt>
		<dd>
			<?php echo h($property['Property']['id']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Size'); ?></dt>
		<dd>
			<?php echo h($property['Property']['size']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Bedrooms'); ?></dt>
		<dd>
			<?php echo h($property['Property']['bedrooms']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Bathrooms'); ?></dt>
		<dd>
			<?php echo h($property['Property']['bathrooms']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Garages'); ?></dt>
		<dd>
			<?php echo h($property['Property']['garages']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Price'); ?></dt>
		<dd>
			<?php echo h($property['Property']['price']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Yearbuilt'); ?></dt>
		<dd>
			<?php echo h($property['Property']['yearbuilt']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Rent'); ?></dt>
		<dd>
			<?php echo h($property['Property']['rent']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Sale'); ?></dt>
		<dd>
			<?php echo h($property['Property']['sale']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Description'); ?></dt>
		<dd>
			<?php echo h($property['Property']['description']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Created'); ?></dt>
		<dd>
			<?php echo h($property['Property']['created']); ?>
			&nbsp;
		</dd>
		<dt><?php echo __('Modified'); ?></dt>
		<dd>
			<?php echo h($property['Property']['modified']); ?>
			&nbsp;
		</dd>
	</dl>
</div>
<div class="actions">
	<h3><?php echo __('Actions'); ?></h3>
	<ul>
		<li><?php echo $this->Html->link(__('Edit Property'), array('action' => 'edit', $property['Property']['id'])); ?> </li>
		<li><?php echo $this->Form->postLink(__('Delete Property'), array('action' => 'delete', $property['Property']['id']), array(), __('Are you sure you want to delete # %s?', $property['Property']['id'])); ?> </li>
		<li><?php echo $this->Html->link(__('List Properties'), array('action' => 'index')); ?> </li>
		<li><?php echo $this->Html->link(__('New Property'), array('action' => 'add')); ?> </li>
	</ul>
</div>
