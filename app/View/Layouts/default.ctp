<!DOCTYPE html>
<html lang="en">
<?=$this->element('header');?>

<?=$this->element('breadcrumps');?>

</header>

<!-- begin Content -->
<section>

<div class="container">

<!-- variabele content -->
<!-- als je naar http://localhost/vastgoed2/properties surft wordt hier de view van properties getoond, als je een andere view aanspreekt, wordt die data hier getoond -->
<!-- http://localhost/vastgoed2/properties verwijst naar de view van het properties Model-->
<?=$this->fetch('content');?>

</div>

</section>
<!-- end Content -->

<?=$this->element('footer');?>

</body>
</html>