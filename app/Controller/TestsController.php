<?php
/**
 * Static content controller.
 *
 * This file will render views from views/pages/
 *
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright     Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link          http://cakephp.org CakePHP(tm) Project
 * @package       app.Controller
 * @since         CakePHP(tm) v 0.2.9
 * @license       http://www.opensource.org/licenses/mit-license.php MIT License
 */

App::uses('AppController', 'Controller');

/**
 * Static content controller
 *
 * Override this controller by placing a copy in controllers directory of an application
 *
 * @package       app.Controller
 * @link http://book.cakephp.org/2.0/en/controllers/pages-controller.html
 */
class TestsController extends AppController {

    /**
     * This controller does not use a model
     *
     * @var array
     */
    //public $uses = array(); //deze weglaten als er niet met een model gewerkt worddt. als we hier vb wel een model zouden gebruiken, zou je hier '$uses = array('property') moeten zetten en zou je controller PropertiesController heten
    //Deze PagesController dient voor code die niet gelinkt is aan een model.

    public $uses = array('Property','Image'); //Property is het model, als je de controller PropertiesController zou noemen zou je deze ook mogen weglaten

   public function test() {
       //http://localhost/vastgoed2/tests/test
       //$this->set('tekstje','Dit is een tekst');
       //$eerste = $this->Property->find('first');  //haal de eerste record uit model Property, geeft een array terug = OPHALEN
      /* $eerste = $this->Property->find('first',
                                            array(
                                                'order' => array('id DESC'),  //zoek eerste property, order by id desc
                                                'conditions' => array('id' => '1')//where clause, where id = '1', mag altijd tussen quotes
                                            ));*/

       $eerste = $this->Property->find('all',
           array(
               'recursive' => 0, //met recursive kan je aan of uit zetten of je de gegevens uit gelinkte tabellen (in dit geval images) wel of niet wil ophalen. 0 is uit
               'conditions' => array('id' => '1'),
               'contain' => array('Image') //je wil enkel de gegevens uit de gelinkte tabel 'Image' ophalen, voor alle andere gelinkte tabellen staat recursive op 0
               //!!JE moet hiervoor wel in elke class die ja apart wil kunnen containen volgende lijn zetten : public $actsAs = array('Containable'); => best zetten in je AppModel.php, dan moet je hem maar 1 x zetten
           )); //doordat we nu een link tussen properties en images hebben wordt nu ook de image opgehaald (als er een is)

       //fields : om enkel bepaalde kolommen te selecteren uit een model ipv allemaal  vb array('Property.id');

       //Zie ook Magic Find Types op http://book.cakephp.org/2.0/en/models/retrieving-your-data.html

       $this->set('eerste',$eerste); //=DOORSTUREN NAAR VIEW

   //    $list = $this->Property->find('list'); //per kolom die je hebt kan je alle gegevens gaan opvragen vb een list van garage = een lijst van alle garages, handig om met lists te gaan werken, standaard geeft dit id terug

     //  $this->set('list',$list);


       //voor deze moeten we hier bovenaan bij uses ook 'Images' toevoegen, anders krijgen we een fatal arror
     /*  $tweede = $this->Image->find('first',
           array(
               'conditions' => array('Image.id' => '1') //tabelnaam voor id zetten, omdat er ook in properties een id zit. Anders krijg je een foutmelding
           ));*/ //doordat we nu een link tussen properties en images hebben wordt nu ook de image opgehaald (als er een is)

       //fields : om enkel bepaalde kolommen te selecteren uit een model ipv allemaal  vb array('Property.id');

       //Zie ook Magic Find Types op http://book.cakephp.org/2.0/en/models/retrieving-your-data.html

//       $this->set('tweede',$tweede);
   }

    public function testje(){
        $woningen = $this->Property->find('all',
            array(
                'recursive' => -1 //met recursive kan je aan of uit zetten of je de gegevens uit gelinkte tabellen (in dit geval images) wel of niet wil ophalen. 0 is uit
                          ));

        //var_dump($woningen);

        $this->set('properties',$woningen);

    }

    public function delete($id){
        $this->Property->delete($id);
        //redirect naar test view
        return $this->redirect(array('action' => 'testje'));
            //als je naar een andere controller zou willen moet je de controller er bij zetten : return $this->redirect(array('controller' => 'tests', 'action' => 'testje'));
    }


}
