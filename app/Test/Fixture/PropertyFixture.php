<?php
/**
 * PropertyFixture
 *
 */
class PropertyFixture extends CakeTestFixture {

/**
 * Fields
 *
 * @var array
 */
	public $fields = array(
		'id' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false, 'key' => 'primary'),
		'size' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'bedrooms' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'bathrooms' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'garages' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'price' => array('type' => 'decimal', 'null' => false, 'default' => null, 'length' => 10, 'unsigned' => false),
		'yearbuilt' => array('type' => 'integer', 'null' => false, 'default' => null, 'unsigned' => false),
		'rent' => array('type' => 'boolean', 'null' => false, 'default' => null),
		'sale' => array('type' => 'boolean', 'null' => false, 'default' => null),
		'description' => array('type' => 'string', 'null' => false, 'default' => null, 'length' => 500, 'collate' => 'utf8_bin', 'charset' => 'utf8'),
		'created' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'modified' => array('type' => 'datetime', 'null' => false, 'default' => null),
		'indexes' => array(
			'PRIMARY' => array('column' => 'id', 'unique' => 1)
		),
		'tableParameters' => array('charset' => 'utf8', 'collate' => 'utf8_bin', 'engine' => 'InnoDB')
	);

/**
 * Records
 *
 * @var array
 */
	public $records = array(
		array(
			'id' => 1,
			'size' => 1,
			'bedrooms' => 1,
			'bathrooms' => 1,
			'garages' => 1,
			'price' => '',
			'yearbuilt' => 1,
			'rent' => 1,
			'sale' => 1,
			'description' => 'Lorem ipsum dolor sit amet',
			'created' => '2014-10-24 20:57:18',
			'modified' => '2014-10-24 20:57:18'
		),
	);

}
